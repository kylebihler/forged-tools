<?php

/**
 * Plugin Name: Forged Tools
 * Description: A collection of tools built by Kyle Bihler.
 * Version: 1.0.4
 * Author: Kyle Bihler
 */

//block direct access to this plugins PHP files
defined('ABSPATH') or die('I pity tha fool who puts script here!');

define( 'FORGED_TOOLS_URL', plugin_dir_url( __FILE__ ) );
if ( ! defined( 'FORGED_TOOLS_TEXTDOMAIN' ) ) {
    define( 'FORGED_TOOLS_TEXTDOMAIN','login-customizer' );
}

function register_forged_tools_styles()
{
    wp_enqueue_style('forged-tools', plugin_dir_url(__FILE__) . '/css/forged-tools.css');
    wp_enqueue_script('forged-tools', plugin_dir_url(__FILE__) . '/js/forged-tools.js');
}

add_action('wp_enqueue_scripts', 'register_forged_tools_styles');

function get_forged_tools_copyright()
{
    ob_start();
    include("includes/forged-copyright.php");
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

add_shortcode('forged_copyright','get_forged_tools_copyright');

function forged_tools_customize_register($wp_customize)
{
    $wp_customize->add_panel('forged_tools_panel', array(
        'priority' => 30,
        'capability' => 'edit_theme_options',
        'title' => __('Forged Theme Options', FORGED_TOOLS_TEXTDOMAIN),
        'description' => __('This section allows you to change color of the copyright on your website.', FORGED_TOOLS_TEXTDOMAIN),
    ));

    $wp_customize->add_section('forged_tools_copyright_section', array(
        'priority' => 10,
        'title' => __('Copyright', FORGED_TOOLS_TEXTDOMAIN),
        'panel'  => 'forged_tools_panel',
    ));

    $wp_customize->add_setting('forged_tools_copyright_color', array(
        'default' => '#F1F1F1',
        'type' => 'option',
        'capability' => 'edit_theme_options',
    ));

    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'forged_tools_copyright_color', array(
        'label' => __('Color', FORGED_TOOLS_TEXTDOMAIN),
        'section' => 'forged_tools_copyright_section',
        'priority' => 10,
        'settings' => 'forged_tools_copyright_color'
    )));
}

add_action('customize_register', 'forged_tools_customize_register');

function forged_tools_register_options_page() {
    $forged_icon = "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA3Mi42NzU4NCA2OS4xODQ5OTkiIHdpZHRoPSI3Mi42NzYiIGhlaWdodD0iNjkuMTg1Ij48cGF0aCBkPSJNNjIuNDE3IDMxLjM1N3YtNC43MDJIMTQuOTczdjQuNzAySC4wMTJzLS40MjcgOC4xMjIgMy44NDcgMTEuOTY4YzQuNjkgNC4yMjIgMTEuMTE0IDUuOTg0IDExLjExNCA1Ljk4NHYyLjk5Mmg4LjM2OWMuNTQ1LjUzOS45NTUgMS4zNDEuOTU1IDIuNTQ2IDAgMi4xMTctMS40MyAyLjkyLTIuNTE1IDMuMjI1aC02LjgxdjExLjExM2g0Ny40NDVWNTguMDcySDU0LjUxcy0xLjA3LTExLjMyNyA3LjkwNi0xNy43MzhjMy42OTgtMi42NDMgMTAuMjU5LTMuODQ4IDEwLjI1OS0zLjg0OHYtNS4xMjloLTEwLjI2em0tMzIuMzYtNy41NmwxMS41ODgtMi4zNTYtLjUyLTcuNjQ2IDIyLjUwMi0zLjUtMS45NzctOS4yOTMtMjEuODU2IDYuMTY1TDM3LjM4OC4yNzggMjUuODAyIDIuNjM1bDEuMTUgMTAuNzg4IDMuMTA2IDEwLjM3NHpNMTQuODQ0IDExLjQ1OGw5LjE2NyAxMmgyLjgzM2wtNS4yNS0xMC4yMDggMi4yNy4wNjUtNC41Mi0xMS41MjMtNy41LjMzMyA1LjE2NyA5LjE2Ny0yLjE2Ny4xNjZ6TTkuMTc2IDguNzkybC00LjY2Ni4xNjYgMTYuMTY3IDE0TDkuMTc2IDguNzkyem0yLjY2Ny0yLjU0Mkw4LjA5MyAwSDMuNDY4bDUuMTI1IDYuMjVoMy4yNXptNCAxNy4yMDhsLTYuNS0zLjk1OC0xLjY4OCAzLjk1OGg4LjE4OHoiLz48L3N2Zz4=";

    add_menu_page(__('Forged Tools',FORGED_TOOLS_TEXTDOMAIN), __('Forged Tools',FORGED_TOOLS_TEXTDOMAIN), 'manage_options', 'forged_tools_options', 'forged_tools_options_page',$forged_icon,4.5);
}
add_action('admin_menu', 'forged_tools_register_options_page');

function forged_tools_admin_style() {
    wp_enqueue_style( 'admin_style', FORGED_TOOLS_URL . '/css/admin.css',array(), FORGED_TOOLS_VERSION, false );
}
add_action( 'admin_enqueue_scripts', 'forged_tools_admin_style' );

function forged_tools_options_page() { ?>
    <div class="wrap">
        <div id="forged_tools-logo"></div>
        <h2><?php _e('Forged Tools', FORGED_TOOLS_TEXTDOMAIN); ?></h2>
        <h3><?php _e('Time to get creative!', FORGED_TOOLS_TEXTDOMAIN); ?></h3>
        <p><?php _e('To use the Foundry Copyright footer use shortcode [forged_copyright]', FORGED_TOOLS_TEXTDOMAIN); ?></p>
        <p><?php _e('Customize settings right from the Customizer.', FORGED_TOOLS_TEXTDOMAIN); ?></p>
        <p><?php _e('Within Customizer, go to Forged Tools', FORGED_TOOLS_TEXTDOMAIN); ?>.</p>
        <a href="<?php echo get_admin_url(); ?>customize.php" id="submit" class="button button-primary"><?php _e('Customize Forged Tools', FORGED_TOOLS_TEXTDOMAIN); ?></a>
        <p> <a href="https://www.kylebihler.com/?utm_source=<?php echo get_bloginfo('name');?>&utm_medium=Plugin" target="_blank" rel="nofollow">FORGED</a>.</p>
    </div>
    <?php
}