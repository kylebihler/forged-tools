<?php
function forged_tools_register_options_page() {
    add_theme_page(__('Login Customizer',FORGED_TOOLS_TEXTDOMAIN), __('Login Customizer',FORGED_TOOLS_TEXTDOMAIN), 'manage_options', 'forged_tools_options', 'forged_tools_options_page');
}
add_action('admin_menu', 'forged_tools_register_options_page');

function forged_tools_admin_style() {
    wp_enqueue_style( 'admin_style', FORGED_TOOLS_URL . '/css/admin.css',array(), FORGED_TOOLS_VERSION, false );
}
add_action( 'admin_enqueue_scripts', 'forged_tools_admin_style' );


function forged_tools_options_page() { ?>
    <div class="wrap">
        <div id="forged_tools-logo"></div>
        <h2><?php _e('Login Customizer', FORGED_TOOLS_TEXTDOMAIN); ?></h2>
        <h3><?php _e('Howdy!', FORGED_TOOLS_TEXTDOMAIN); ?></h3>
        <p><?php _e('Login Customizer plugin allows you to easily customize your login page straight from your WordPress Customizer! You can preview your changes before you save them! Awesome, right?', FORGED_TOOLS_TEXTDOMAIN); ?></p>
        <p><?php _e('In Customizer, navigate to Login Customizer', FORGED_TOOLS_TEXTDOMAIN); ?>.</p>
        <a href="<?php echo get_admin_url(); ?>customize.php?url=<?php echo wp_login_url(); ?>" id="submit" class="button button-primary"><?php _e('Start Customizing!', FORGED_TOOLS_TEXTDOMAIN); ?></a>
        <h3><?php _e('Credits/Support (All the unwanted crap)', FORGED_TOOLS_TEXTDOMAIN); ?></h3>
        <p><?php _e('If you find any issues or if you want to contribute, then please free to drop me a mail at', FORGED_TOOLS_TEXTDOMAIN); ?> <a href="https://themeisle.com/contact" target="_blank" rel="nofollow"><?php _e('this link', FORGED_TOOLS_TEXTDOMAIN); ?></a>.</p>
        <p><?php _e('Thanks for using this plugin. Don not forget to leave a review.', FORGED_TOOLS_TEXTDOMAIN); ?></p>
        <p> <a href="https://www.forgedideas.com/" target="_blank" rel="nofollow">FORGED</a>.</p>

    </div>
    <?php
}


function forged_tools_dashboard_widget() {
    wp_add_dashboard_widget( 'forged_tools_subscribe_widget', __( 'Subscribe', FORGED_TOOLS_TEXTDOMAIN ),'forged_tools_subscribe_widget');
}
add_action( 'wp_dashboard_setup', 'forged_tools_dashboard_widget' );